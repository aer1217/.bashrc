launch Terminal, type 

```
gedit .bashrc
```

Add the end of the file, type:


```
source /opt/ros/jade/setup.bash
source /home/carre/aer1217/extras/devel/setup.bash
source /home/carre/aer1217/labs/devel/setup.bash
export GAZEBO_PLUGIN_PATH=$HOME/aer1217/gazebo/plugins
export GAZEBO_MODEL_PATH=$HOME/aer1217/gazebo/models
export GAZEBO_RESOURCE_PATH=$HOME/aer1217/gazebo/world
```

Note: these lines might have been added if you ran the scripts from https://bitbucket.org/aer1217/setup_script/src/